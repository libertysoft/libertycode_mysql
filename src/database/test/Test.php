<?php
/**
 * GET arguments:
 * - clean: = 1: Drop test database at the end.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;
use liberty_code\mysql\database\connection\pdo\model\MysqlPdoConnection;



// Init var
$boolDbExists = false;

$strDbNm = 'libertycode_sql_test';

$strTableNmUsr = 'user';
$strColNmUsrId = 'usr_id';
$strColNmUsrLg = 'usr_login';
$strColNmUsrNm = 'usr_name';
$strColNmUsrFnm = 'usr_first_name';
$strColNmUsrEmail = 'usr_email';

$strTableNmMsg = 'message';
$strColNmMsgId = 'msg_id';
$strColNmMsgDateCreate = 'msg_dt_create';
$strColNmMsgTxt = 'msg_txt';
$strColNmMsgUsr = 'msg_usr_id';

$tabConfig = array(
    ConstConnection::TAB_CONFIG_KEY_HOST => 'localhost',
    ConstConnection::TAB_CONFIG_KEY_CHARSET => 'utf8',
    ConstConnection::TAB_CONFIG_KEY_LOGIN => 'root',
    ConstConnection::TAB_CONFIG_KEY_PASSWORD => '',
    ConstPdoConnection::TAB_CONFIG_KEY_OPTION => [
        //PDO::ATTR_AUTOCOMMIT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT
    ]
);
$objConnection = new MysqlPdoConnection($tabConfig);



// Test config
echo('Test config: <br />');
echo('Config: <pre>');var_dump($objConnection->getTabConfig());echo('</pre>');
echo('<br /><br /><br />');



// Test connection
echo('Test connection: <br />');
echo('Is connected: <pre>');var_dump($objConnection->checkIsConnect());echo('</pre>');

$objConnection->close();
echo('Is connected (prior disconnection): <pre>');var_dump($objConnection->checkIsConnect());echo('</pre>');

$objConnection->connect();
echo('Is connected (prior connection): <pre>');var_dump($objConnection->checkIsConnect());echo('</pre>');

echo('<br /><br /><br />');



// Test execute result
echo('Test execute result: <br />');

$strSql = "SHOW DATABASES;";
$objResult = $objConnection->executeResult($strSql);

echo('SQL: ' . $strSql . '<br />');
echo('Count row: <pre>');var_dump($objResult->getIntCountRow());echo('</pre>');
echo('Count column: <pre>');var_dump($objResult->getIntCountCol());echo('</pre>');

$data = $objResult->getFetchData();
if($data !== false)
{
    echo('Fetching 1st row: <pre>');var_dump($data);echo('</pre>');
}

$tabData = $objResult->getTabData();
echo('TabData: <pre>');var_dump($tabData);echo('</pre>');

$data = $objResult->getFetchData();
if($data !== false)
{
    echo('Fetching last row: <pre>');var_dump($data);echo('</pre>');
}
else
{
    echo('Fetching last row: Nothing<br />');
}
echo('<br /><br /><br />');

echo('Re-fetching row:<br />');
$objResult->initialize();
while(($data = $objResult->getFetchData()) !== false)
{
    $boolDbExists = $boolDbExists || ($data['Database'] == $strDbNm);
    echo($data['Database'] . '<br>');
}

$objResult->close();

echo('<br /><br /><br />');



if(!$boolDbExists)
{
    // Test create database
    echo('Test database creation: <br />');

    $strSql = "CREATE DATABASE " . $objConnection->getStrEscapeName($strDbNm) . ";";

    echo('SQL: ' . $strSql . '<br />');
    echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');

    echo('<br /><br /><br />');



    // Test connection to database
    echo('Test database connection: <br />');

    $tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;
    $objConnection->setConfig($tabConfig);

    echo('Config: <pre>');var_dump($objConnection->getTabConfig());echo('</pre>');
    echo('Is connected: <pre>');var_dump($objConnection->checkIsConnect());echo('</pre>');

    echo('<br /><br /><br />');



    // Test build database
    echo('Test database build: <br />');

    echo('In transaction: <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');
    $objConnection->transactionStart();
    echo('In transaction(prior start transaction): <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');

    try
    {
        // Create user table
        $strSql =
            "CREATE TABLE " . $objConnection->getStrEscapeName($strTableNmUsr) . " (
            " . $objConnection->getStrEscapeName($strColNmUsrId) . " int(10) NOT NULL AUTO_INCREMENT,
            " . $objConnection->getStrEscapeName($strColNmUsrLg) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrNm) . " varchar(200) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrFnm) . " varchar(200) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrEmail) . " varchar(200) COLLATE utf8_bin,
        UNIQUE KEY " . $objConnection->getStrEscapeName($strColNmUsrId) . " (" . $objConnection->getStrEscapeName($strColNmUsrId) . ")
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";

        echo('SQL: ' . $strSql . '<br />');
        echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');

        // Create message table
        $strSql =
            "CREATE TABLE " . $objConnection->getStrEscapeName($strTableNmMsg) . " (
            " . $objConnection->getStrEscapeName($strColNmMsgId) . " int(10) NOT NULL AUTO_INCREMENT,
            " . $objConnection->getStrEscapeName($strColNmMsgDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmMsgTxt) . " text,
            " . $objConnection->getStrEscapeName($strColNmMsgUsr) . " int(10) NOT NULL,
        UNIQUE KEY " . $objConnection->getStrEscapeName($strColNmMsgId) . " (" . $objConnection->getStrEscapeName($strColNmMsgId) . ")
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";

        echo('SQL: ' . $strSql . '<br />');
        echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');

        echo('In transaction(prior commit transaction): <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');
        $objConnection->transactionEndCommit();
        echo('In transaction(after commit transaction): <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');
    }
    catch(\Exception $e)
    {
        $objConnection->transactionEndRollback();
        echo('In transaction(prior rollback transaction): <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');
        echo('Error: <pre>');var_dump($e->getMessage());echo('</pre>');
        echo('In transaction(after rollback transaction): <pre>');var_dump($objConnection->checkInTransaction());echo('</pre>');
    }

    echo('<br /><br /><br />');
}



// Re-connect to database
$tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;
$objConnection->setConfig($tabConfig);



// Test insert user
echo('Test insert user: <br />');
$strSql =
    "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmUsr) . " (
        " . $objConnection->getStrEscapeName($strColNmUsrLg) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrNm) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrFnm) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrEmail) . "
    ) VALUES (
        " . $objConnection->getStrEscapeValue('lg_1') . ", 
        " . $objConnection->getStrEscapeValue('NM 1') . ", 
        " . $objConnection->getStrEscapeValue('Fnm 1') . ", 
        " . $objConnection->getStrEscapeValue(null) . "
    ), (
        " . $objConnection->getStrEscapeValue('lg_2') . ", 
        " . $objConnection->getStrEscapeValue('NM 2') . ", 
        " . $objConnection->getStrEscapeValue('Fnm 2') . ", 
        " . $objConnection->getStrEscapeValue(2) . "
    );";
echo('SQL: ' . $strSql . '<br />');
echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');
$strUsrId1 = $objConnection->getStrLastInsertId();
echo('Inserted id: <pre>');var_dump($strUsrId1);var_dump($objConnection->getStrLastInsertId());echo('</pre>');

$strSql =
    "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmUsr) . " (
        " . $objConnection->getStrEscapeName($strColNmUsrLg) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrNm) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrFnm) . ", 
        " . $objConnection->getStrEscapeName($strColNmUsrEmail) . "
    ) VALUES (
        " . $objConnection->getStrEscapeValue('lg_3') . ", 
        " . $objConnection->getStrEscapeValue('NM 3') . ", 
        " . $objConnection->getStrEscapeValue('Fnm 3') . ", 
        " . $objConnection->getStrEscapeValue('fnm3.nm3@test.com') . "
    );";
echo('SQL: ' . $strSql . '<br />');
echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');
$strUsrId3 = $objConnection->getStrLastInsertId();
echo('Inserted id: <pre>');var_dump($strUsrId3);var_dump($objConnection->getStrLastInsertId());echo('</pre>');

echo('<br /><br /><br />');



// Test insert message
echo('Test insert message: <br />');
$strSql =
    "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmMsg) . " (
        " . $objConnection->getStrEscapeName($strColNmMsgDateCreate) . ", 
        " . $objConnection->getStrEscapeName($strColNmMsgTxt) . ", 
        " . $objConnection->getStrEscapeName($strColNmMsgUsr) . "
    ) VALUES (
        now(), 
        :Txt, 
        :UsrId
    );";
$objStatement = $objConnection->getObjStatement($strSql);
echo('SQL: ' . $objStatement->getStrCommand() . '<br />');

echo('Execution: <pre>');var_dump($objStatement->execute(array(
    'Txt' => "Txt' 1",
    'UsrId' => intval($strUsrId1)
)));
echo('Config: <pre>');var_dump($objStatement->getTabParam());echo('</pre>');

echo('Execution: <pre>');var_dump($objStatement->execute(array(
    'Txt' => 'Txt 2',
    'UsrId' => intval($strUsrId1)
)));
echo('Config: <pre>');var_dump($objStatement->getTabParam());echo('</pre>');

echo('Execution: <pre>');var_dump($objStatement->execute(array(
    'Txt' => "Txt' 3",
    'UsrId' => strval($strUsrId1)
)));
echo('Config: <pre>');var_dump($objStatement->getTabParam());echo('</pre>');

echo('Execution: <pre>');var_dump($objStatement->execute(array(
    'Txt' => null,
    'UsrId' => intval($strUsrId3)
)));
echo('Config: <pre>');var_dump($objStatement->getTabParam());echo('</pre>');

echo('Execution: <pre>');var_dump($objStatement->execute(array(
    'Txt' => "Txt' 5",
    'UsrId' => strval($strUsrId3)
)));
echo('Config: <pre>');var_dump($objStatement->getTabParam());echo('</pre>');

echo('<br /><br /><br />');



// Test select message
echo('Test select message: <br />');

$strSql =
    "SELECT * 
    FROM " . $objConnection->getStrEscapeName($strTableNmMsg) . " 
    WHERE (" . $objConnection->getStrEscapeName($strColNmMsgUsr) . "  = :UsrId);";
$objStatement = $objConnection->getObjStatement($strSql);
echo('SQL: ' . $objStatement->getStrCommand() . '<br />');

$objStatement->setParam(array('UsrId' => $strUsrId1));
$objResult = $objStatement->executeResult();
echo('Count row: <pre>');var_dump($objResult->getIntCountRow());echo('</pre>');
echo('Count column: <pre>');var_dump($objResult->getIntCountCol());echo('</pre>');

for($intCptRow = 0; ($intCptRow < $objResult->getIntCountRow()); $intCptRow++)
{
    for($intCptCol = 0; ($intCptCol < $objResult->getIntCountCol()); $intCptCol++)
    {
        echo('Data "' . $objResult->getStrColName($intCptCol) . '": <pre>');
        var_dump($objResult->getRowData($intCptRow, $intCptCol));
        echo('</pre>');
        echo('<br />');
    }
    echo('<br />');
}

$objResult->close();

echo('<br /><br /><br />');

$objResult = $objStatement->executeResult(array('UsrId' => $strUsrId3));
echo('Count row: <pre>');var_dump($objResult->getIntCountRow());echo('</pre>');
echo('Count column: <pre>');var_dump($objResult->getIntCountCol());echo('</pre>');

for($intCptRow = 0; ($intCptRow < $objResult->getIntCountRow()); $intCptRow++)
{
    for($intCptCol = 0; ($intCptCol < $objResult->getIntCountCol()); $intCptCol++)
    {
        echo('Data "' . $objResult->getStrColName($intCptCol) . '": <pre>');
        var_dump($objResult->getRowData($intCptRow, $intCptCol));
        echo('</pre>');
        echo('<br />');
    }
    echo('<br />');
}

$objResult->close();

echo('<br /><br /><br />');



if(trim(ToolBoxTable::getItem($_GET, 'clean', '0')) == '1')
{
    // Re-connect to database
    unset($tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME]);
    $objConnection->setConfig($tabConfig);



    // Test remove database
    echo('Test database remove: <br />');

    $strSql = "DROP DATABASE " . $objConnection->getStrEscapeName($strDbNm) . ";";

    echo('SQL: ' . $strSql . '<br />');
    echo('Execution: <pre>');var_dump($objConnection->execute($strSql));echo('</pre>');

    echo('<br /><br /><br />');
}



// Close connection
$objConnection->close();


