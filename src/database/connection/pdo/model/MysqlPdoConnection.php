<?php
/**
 * Description :
 * This class allows to define PDO MySQL database connection class.
 * Based on MySQL PDO connection, to design connection and request interface.
 * PDO MySQL database connection allows to specify following configuration:
 * [
 *     PDO SQL database connection configuration,
 *
 *     driver: "mysql"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\mysql\database\connection\pdo\model;

use liberty_code\sql\database\connection\pdo\model\PdoConnection;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;
use liberty_code\mysql\database\connection\pdo\library\ConstMysqlPdoConnection;



class MysqlPdoConnection extends PdoConnection
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrConfigDsn()
    {
        // Init var
        $result = '';
        $tabInfo = array(
            [
                ConstConnection::TAB_CONFIG_KEY_HOST,
                ConstMysqlPdoConnection::DSN_CONFIG_PATTERN_HOST
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_PORT,
                ConstMysqlPdoConnection::DSN_CONFIG_PATTERN_PORT
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_DB_NAME,
                ConstMysqlPdoConnection::DSN_CONFIG_PATTERN_DB_NAME
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_CHARSET,
                ConstMysqlPdoConnection::DSN_CONFIG_PATTERN_CHARSET
            ]
        );

        // Run all info
        foreach($tabInfo as $info)
        {
            // Get info
            $strConfigKey = $info[0];
            $strPattern = $info[1];

            // Get value
            $strValue = strval(ToolBoxTable::getItem(
                $this->getTabConfig(),
                $strConfigKey,
                ''
            ));

            // Format value
            $strValue = (
            (trim($strValue) != '') ?
                sprintf($strPattern, $strValue) :
                ''
            );

            // Register in result, if required
            if(trim($strValue) != '')
            {
                if(trim($result) == '')
                {
                    $result = $strValue;
                }
                else
                {
                    $result = sprintf(
                        ConstMysqlPdoConnection::DSN_CONFIG_PATTERN_LINK,
                        $result,
                        $strValue
                    );
                }
            }
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig, $boolConnect = true)
    {
        // Set MySQL driver
        $tabConfig[ConstPdoConnection::TAB_CONFIG_KEY_DRIVER] = ConstMysqlPdoConnection::DRIVER_CONFIG_MYSQL;

        // Call parent method
        parent::setConfig($tabConfig, $boolConnect);
    }



}