<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\mysql\database\connection\pdo\library;



class ConstMysqlPdoConnection
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Driver configuration
    const DRIVER_CONFIG_MYSQL = 'mysql';

    // DSN configuration
    const DSN_CONFIG_PATTERN_LINK = '%1$s;%2$s';
    const DSN_CONFIG_PATTERN_HOST = 'host=%s';
    const DSN_CONFIG_PATTERN_PORT = 'port=%s';
    const DSN_CONFIG_PATTERN_DB_NAME = 'dbname=%s';
    const DSN_CONFIG_PATTERN_CHARSET = 'charset=%s';
}