LibertyCode_MySql
=================



Description
-----------

Library contains MySQL components, 
to use SQL components for 
MySQL data storage.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/mysql ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/mysql": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### MySQL database connection

Connection allows to design MySQL database connection,
to connect and request on specific MySQL data storage, 
from specified configuration.

_Elements_

- MysqlPdoConnection

    Extends PDO connection features, 
    Allows to design a MySQL database connection, 
    using specific PDO standard.
    
_Example_

```php
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\mysql\database\connection\pdo\model\MysqlPdoConnection;
...
// Get connection
$connection = new MysqlPdoConnection(array(
    ConstConnection::TAB_CONFIG_KEY_HOST => 'host',
    ConstConnection::TAB_CONFIG_KEY_DB_NAME => 'db_name'
    ConstConnection::TAB_CONFIG_KEY_CHARSET => 'utf8',
    ConstConnection::TAB_CONFIG_KEY_LOGIN => 'login',
    ConstConnection::TAB_CONFIG_KEY_PASSWORD => 'password'
));
...
// Execute SQL command
$connection->execute('...SQL string command');
...
```

---


